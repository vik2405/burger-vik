import React from 'react';
import './Burger.css';
import Ingredient from "./Ingredient/Ingredient";

const Burger = props => {

    const ingredientKeys = Object.keys(props.ingredients);
    // ['meat', 'cheese']

    let ingredients = [
        /*<Ingredient key="meat" type="meat" />,
        <Ingredient key="salad" type="salad" />,
        <Ingredient key="bacon" type="bacon" />,
        <Ingredient key="cheese" type="cheese" />*/
    ];

    ingredientKeys.forEach(igKey => {
        let amount = props.ingredients[igKey]; //4
        for (let i = 0; i< amount; i++) {
            ingredients.push(<Ingredient type={igKey}/>)
        }
    });
    if (ingredients.length === 0) {
        ingredients = <p>Please start adding ingredients!</p>
    }

    return (
    <div className="Burger">
        <Ingredient type="bread-top" />
        {ingredients}
        <Ingredient type="bread-bottom" />
    </div>
    )
};



export default Burger;